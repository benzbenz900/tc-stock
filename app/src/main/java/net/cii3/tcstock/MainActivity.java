package net.cii3.tcstock;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import android.app.ProgressDialog;
import android.widget.ImageView;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    EditText emailBox, passwordBox;
    Button loginButton;
    ImageView imageView;
    String URL = "https://tc.kck.co.th/rest/postLogin/";
    String username,user_level,store_id,lock_user;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView2);

        Glide.with(MainActivity.this)
                .load("https://www.kroko-cosmetics.com/assets/img/logotran+.png")
                .into(imageView);


        sp = getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);
        username = sp.getString("username", "username");
        user_level = sp.getString("user_level", "user_level");
        store_id = sp.getString("store_id", "store_id");
        lock_user = sp.getString("lock_user", "lock_user");

        if(!username.equals("username") && !user_level.equals("user_level") && !store_id.equals("store_id") && !lock_user.equals("lock_user")){

            final ProgressDialog loadingDialog = ProgressDialog.show(MainActivity.this, getLang("Loading"), getLang("Login"), true, false);

            new Thread() {
                public void run() {
                    try{
                        sleep(2000);
                    } catch (Exception ignored) {

                    }

                    new Thread() {
                        public void run() {

                            loadingDialog.dismiss();

                            Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }.start();
                }
            }.start();



        }

        emailBox = findViewById(R.id.emailBox);
        emailBox.requestFocus();
        emailBox.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER))
                {
                    // Perform action on Enter key press
                    emailBox.clearFocus();
                    passwordBox.requestFocus();
                    return true;
                }
                return false;
            }
        });
        passwordBox = findViewById(R.id.passwordBox);
        passwordBox.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER))
                {
                    loginCall();
                    return true;
                }
                return false;
            }
        });

        loginButton = findViewById(R.id.loginButton);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginCall();
            }
        });

    }

    void loginCall(){
        final ProgressDialog loadingDialog = ProgressDialog.show(MainActivity.this, getLang("Loading"), getLang("Login"), true, false);
        StringRequest request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>(){
            @Override
            public void onResponse(String s) {
                try {
                    loadingDialog.dismiss();
                    JSONObject obj = new JSONObject(s);
                    String getLogin = obj.getString("getLogin");
                    if(obj.getString("getLogin") != null){
                        JSONObject objLogin = new JSONObject(getLogin);

                        if(objLogin.getString("user_level").equals("store")){

                            sp = getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);
                            editor = sp.edit();

                            editor.putString("username",objLogin.getString("username"));
                            editor.putString("user_level",objLogin.getString("user_level"));
                            editor.putString("store_id",objLogin.getString("store_id"));
                            editor.putString("lock_user",objLogin.getString("lock_user"));

                            editor.apply();

                            Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }else{
                            alertBlock(getLang("only_user_store"));
                        }

                    }else{
                        alertBlock(getLang("Incorrect_Details"));
                    }


                } catch (Throwable t) {
                    alertBlock(getLang("Incorrect_Details"));
                }

            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                alertBlock(getLang("Some_error_occurred")+volleyError);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("username", emailBox.getText().toString());
                parameters.put("password", passwordBox.getText().toString());
                return parameters;
            }
        };

        RequestQueue rQueue = Volley.newRequestQueue(MainActivity.this);
        rQueue.add(request);
    }


    void alertBlock(String text){

        AlertDialog.Builder builder =
                new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(text);
        builder.setPositiveButton("OK",null);
        builder.show();

    }

    String getLang(String text){
        return getResources()
                .getString(
                        getResources()
                                .getIdentifier(
                                        text,
                                        "string",
                                        getPackageName()
                                )
                );
    }
}